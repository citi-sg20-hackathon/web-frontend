import { Injectable } from '@angular/core';
import { Order } from './order/order';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private httpClient: HttpClient) {}

  getAllOrderList(): Observable<Array<Order>> {
    let observerableResult = this.httpClient.get('http://localhost:8081/api/trade') as Observable<Array<Order>>;
    return observerableResult;
  }

  postThisOrder(data) {
    return this.httpClient.post('http://localhost:8081/api/trade', data);
  }
}

