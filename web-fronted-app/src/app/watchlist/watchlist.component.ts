import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';
import { Item } from '../item';
import { timer } from 'rxjs';
@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})

export class WatchlistComponent implements OnInit {

  itemList: Item[];
  // This field holds the data returned from external webservices.itemList: Item[];

  // After dependency injection, this.quotesService can be used.
  constructor(private itemService: ItemService) { }

  ngOnInit() {
  }

}
