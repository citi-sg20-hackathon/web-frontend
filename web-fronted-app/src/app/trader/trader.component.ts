import {Component, OnInit} from '@angular/core';
import { Order } from '../order/order';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from '../orders.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-trader',
  templateUrl: './trader.component.html',
  styleUrls: ['./trader.component.css']
})
export class TraderComponent implements OnInit{
  order: Order;

  constructor( private route: ActivatedRoute,
               private router: Router,
               private httpClient: HttpClient,
               private ordersService: OrdersService ) {
  }

  onSubmit(data) {
    this.ordersService.postThisOrder(data)
      .subscribe((result) => {
        console.warn('result', result);
      });
    console.warn(data);
  }

  ngOnInit(): void {
  }


  // gotoOrderList() {
  //  this.router.navigate(['/orders']);
  // }


/*  updateQuantity($event) {
    this.quantity = Number($event.target.value);
  }

  updatePrice($event) {
    this.price = Number($event.target.value);
  }

  submitOrder() {
    console.log(this.quantity);
    console.log(this.price);
  }*/


}


