import { Component } from '@angular/core';
import { OrdersService } from './orders.service';
import { Order } from './order/order';
import { Observable } from 'rxjs';
import {Item} from "./item";
import {ItemService} from "./item.service";
import {Ticker} from "./ticker";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  orders: Observable<Array<Order>>;
  item: Observable<Item>;
  ticker: Observable<Ticker[]>;

  constructor(ordersService: OrdersService, itemService: ItemService) {
    this.orders = ordersService.getAllOrderList();
    this.ticker = itemService.getTicker();
  }
}



