import DateTimeFormat = Intl.DateTimeFormat;

export class Order {
  constructor(
    public created: Date,
    public state: string,
    public type: string,
    public ticker: string,
    public quantity: number,
    public price: number ) { }
}
