import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Item } from 'src/app/item';
import {ItemService} from '../item.service';
import {Ticker} from '../ticker';

@Component({
  selector: 'app-watchlist-item',
  templateUrl: './watchlist-item.component.html',
  styleUrls: ['./watchlist-item.component.css']
})
export class WatchlistItemComponent{

  @Input() items: any[];
  ticker: Ticker;

  constructor(private itemService: ItemService) {
  }

  ngOnInit() {
      this.itemService.getTicker().subscribe(tickers => {
        tickers.forEach(ticker => { this.itemService.getItem(ticker).subscribe( response => {
            this.items.push({ticker, response } ); }); }); }); }
}
