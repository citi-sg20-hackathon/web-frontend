import {Component} from '@angular/core';
import { Order } from '../order/order';
import { Observable } from 'rxjs';
import { OrdersService } from '../orders.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent {
  displayedColumns: string[] = ['ticker', 'type', 'quantity', 'price', 'state', 'created',];
  orders: Observable<Array<Order>>;
  theOrder: Order;
  constructor(ordersService: OrdersService) {
    this.orders = ordersService.getAllOrderList();
  }
}
