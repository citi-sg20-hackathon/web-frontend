export class Item {

  constructor(
    // public ticker: string,
    public c: number, // current value
    public h: number, // high
    public l: number, // low
    public o: number, // open
    public pc: number){} // previous close
}
