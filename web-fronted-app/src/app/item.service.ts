import { Injectable } from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Item} from './item';
import {Ticker} from './ticker';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  baseUrl = 'http://localhost:8081/api/watch';
  itemUrl = 'http://localhost:8081/api/returns/stock';

  constructor(private http: HttpClient) { }

  getItem(ticker: string): Observable<Item> {
    return this.http.get<Item>(this.itemUrl + '/' + ticker);
  }
  getTicker(): Observable<any> {
    return this.http.get<Ticker[]>(this.baseUrl);
  }
  deleteTicker(ticker: string): Observable<any> {
    const deleteTickerUrl: string = this.baseUrl + '/' + ticker;
    return this.http.delete<void>(deleteTickerUrl, httpOptions);
  }
  addTicker(ticker: string): Observable<any>{
    return this.http.post<void>(this.baseUrl, ticker, httpOptions);
  }
}


