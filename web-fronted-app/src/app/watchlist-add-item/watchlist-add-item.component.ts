import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {OrdersService} from "../orders.service";
import {ItemService} from "../item.service";

@Component({
  selector: 'app-watchlist-add-item',
  templateUrl: './watchlist-add-item.component.html',
  styleUrls: ['./watchlist-add-item.component.css']
})
export class WatchlistAddItemComponent implements OnInit {

  @Output() addSymbol: EventEmitter<any> = new EventEmitter();

  ticker:string;

  constructor(private itemService: ItemService) { }

  ngOnInit() {
  }

  onSubmit(ticker){
    console.warn(ticker);
    this.itemService.addTicker(ticker)
      .subscribe((result) => {
        console.warn('result', result);
      });
    console.warn(ticker);
  }

  onDelete(ticker){
    this.itemService.deleteTicker(ticker)
      .subscribe((result) => {
        console.warn('result', result);
      });
    console.warn(ticker);
  }

}
