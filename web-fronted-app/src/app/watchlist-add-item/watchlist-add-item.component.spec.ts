import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchlistAddItemComponent } from './watchlist-add-item.component';

describe('WatchlistAddItemComponent', () => {
  let component: WatchlistAddItemComponent;
  let fixture: ComponentFixture<WatchlistAddItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WatchlistAddItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchlistAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
